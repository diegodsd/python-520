# TUPLA / LISTA / DICIONÁRIO / SET - Coleções

# tupla = ("Espinafre", "Batata", "Ervilha", 50, True)
# #INDEX        0          1          2       3    4    

# # print(tupla)
# # print(tupla[3])

# for lalala in tupla:
#     # print(lalala)
#     print(f"O objeto {lalala} é um {type(lalala)}")


# lista = ["Espinafre", "Batata", "Ervilha", 50, True]
#INDEX        0          1          2       3    4

#Métodos 
# lista.append("Pão de batata")

# lista.remove(50)

# lista.pop(0)
# lista.insert(2, "Avião")

# print(lista)

# frase = "Testando uma nova frase"
# print(frase.replace("a", "---"))
