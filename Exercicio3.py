# Exercicio 1:
# Escreva uma função que receba um nome e que tenha como saída uma saudação.

# O argumento da função deverá ser o nome, e saída deverá ser como a seguir:

# chamada da função: saudacao('Lalo')
# saída: 'Olá Lalo! Tudo bem com você?'


# def saudacao(nome):
#     print(f"Olá {nome}! Tudo bem com você?")
#     return (saudacao)

# nome = input("Informe um nome: ")
# saudacao(nome)


# Resultado seria:
# Informe um nome: Lalo
# Olá Lalo! Tudo bem com você?


# ----------------------------------------------
# def saudacao(nome):
#     print(f"Olá {nome}! Tudo bem com você?")

# nome = input("Informe um nome: ")
# saudacao(nome)

# # ----------------------------------------------
# def saudacao(nome):
#     return f"Olá {nome}! Tudo bem com você?"

# nome = input("Informe um nome: ")
# print(saudacao(nome))



# ====================================================
# Exercicio 2:
# Escreva uma calculadora utilizando funções
# Ela pergunta dois numeros, e da as opções de calculo.
# (soma, diferença, multiplicação, divisão)

# def soma():
#     x = float(input("Primeiro numero: "))
#     y = float(input("Segundo numero: "))
#     print("Soma: ",x+y)

# def diferenca():
#     x = float(input("Primeiro numero: "))
#     y = float(input("Segundo numero: "))
#     print("Diferenca: ",x-y)

# def multiplicacao():
#     x = float(input("Primeiro numero: "))
#     y = float(input("Segundo numero: "))
#     print("Multiplicacao: ",x*y)

# def divisao():
#     x = float(input("Primeiro numero: "))
#     y = float(input("Segundo numero: "))
#     print("Divisao: ",x/y)

# opcao=1

# while True:
#     print("1. Soma")
#     print("2. Diferença")
#     print("3. Multiplicação")
#     print("4. Divisão")

#     opcao = int(input("Opção: "))

#     if(opcao==1):
#         soma()
#     if(opcao==2):
#         diferenca()
#     if(opcao==3):
#         multiplicacao()
#     if(opcao==4):
#         divisao()
#     break

# ====================================================
# Exercicio 3:
# Reescreva o exercício da quitanda do capítulo 2 separando as operações 
# em funções.



# ====================================================
# Exercicio 4:
# Escreva um programa que possua uma função que conte o
# numero de números pares passados à ela, pelo usuário.

def TotalPar(lista):
    pares = 0
    for num in lista:
        if (num % 2) == 0:
            pares = pares + 1
    return pares


lista = list() 

quant = int(input('Quantos valores haverá na lista ?'))
while quant < 0:
    print('Erro')
    quant = int(input('Quantos valores haverá na lista ?'))

for c in range(quant):
    num = int(input('Valor:'))
    lista.append(num)

print('A quantidade de valores pares é:',TotalPar(lista), 'bye bye!')

# ====================================================
# Exercicio 5:
# Assumindo que uma lata de tinta pinta 3m², escreva um programa
# que possua uma função que receba as dimenções de uma parede,
# passadas pelo usuario, calcule sua área, e mostre uma mensagem
# dizendo quantas latas de tinta seriam necessárias para pintar
# essa parede.
