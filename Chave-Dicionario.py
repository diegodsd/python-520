# chave -> valor

# dicionario = {}

# dicionario["chave"] = "Valor"
# dicionario["Nome"] = "Diego"
# dicionario["Cor"] = "Verde"

# print(dicionario)

#               |------ITEM----|  |----ITEM-----|
# DICIONARIO = {'Nome': 'Diego', 'Cor': 'Verde'}
#                CHAVE : VALOR,   CHAVE: VALOR

# for x in dicionario.values():
#     print(x)

# for x, y in dicionario.items():
#     print(f"Chave: {x}, Valor: {y}")