# no python, toda frase precisa ficar entre aspas.

#print("meu nome é Diego")

# Input é usado para coletar informações do usuário.
# input("qual é o teu nome?")

# Variáveis 

# nome = 'Diego D'
# idade = "23"
# frase = "Uma gota d'agua"
# frase2 = 'Não é tão "alto" assim'

# nome = "Diego D"
# print(nome)

# nome = input("Qual é o teu nome? ")

# print("Bem vindo,", nome,"!")
# print("Bem vindo," + nome+ "!")
# print("Bem vindo,{}".format(nome))
# print(f"Bem vindo {nome}!")

# Tipos primitivos
# String = STR = frases, cadeias de caracteres.
# Float = float = Números reais, números com ponto. Não precisa aspas.
# Integer = int = Números inteiros. Não precisa aspas.
# Boolean = bool = True or false - sempre a primeira lentra em maiúsculo (True, False). Não precisa aspas.

# variavel_str = "Avião azul"
# variavel_str2 = "Está voado"
# variavel_float = 25.7
# variavel_int = 40
# variavel_bool = False

# # print(variavel_str + variavel_str2)
# print(variavel_float + variavel_int)

# idade = int(input("Qual é a tua idade?"))